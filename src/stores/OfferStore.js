import {getRoot, types as t} from 'mobx-state-tree';
import Offer from '../models/Offer';

export default t
  .model('OfferStore', {

    items: t.optional(t.map(Offer), {}),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const remove = (id) => {
      self.items.delete(id);
    };

    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map(v => self.createOrUpdate(v));
        return;
      }

      if (item.product) self.root.productStore.createOrUpdate(item.product);

      self.items.set(item.id, {
        ...item,
        used: item.used === 1,
        discount_start_on: item.discount_start_on ? new Date(item.discount_start_on) : null,
        discount_end_on: item.discount_end_on ? new Date(item.discount_end_on) : null,
        discount_active: item.discount_active === 1,
        product: item.product_id,
        discount_percentage: item.discount_percentage ? +item.discount_percentage : null,
      });
    };

    const clearItems = () => {
      self.items = {};
    };

    return {
      setValue,
      createOrUpdate,
      remove,
      clearItems,
    };

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

  }));
