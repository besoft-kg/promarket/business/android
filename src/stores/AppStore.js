import {getRoot, types as t} from 'mobx-state-tree';
import storage from '../utils/storage';
import Store from '../models/Store';
import User from '../models/User';
import Branch from '../models/Branch';
import {requester} from '../utils/settings';
import Manager from '../models/Manager';

export default t
  .model('AppStore', {

    store: t.maybeNull(Store),
    branch: t.maybeNull(t.reference(Branch)),
    user: t.maybeNull(User),
    manager: t.maybeNull(t.reference(Manager)),
    authenticating: t.optional(t.boolean, false),
    auth_checking: t.optional(t.boolean, false),
    token: t.maybeNull(t.string),
    app_is_ready: t.optional(t.boolean, false),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const signOut = () => {
      self.user = null;
      self.store = null;
      self.branch = null;
      self.auth_checking = false;
      self.authenticating = false;
      self.token = null;
      self.manager = null;

      self.root.branchStore.clearItems();
      self.root.colorStore.clearItems();
      self.root.managerStore.clearItems();
      self.root.offerStore.clearItems();
      self.root.productStore.clearItems();
      self.root.userStore.clearItems();

      storage.remove('token').then();
      storage.remove('manager').then();
      storage.remove('branch').then();
      storage.remove('store').then();
      storage.remove('user').then();
    };
    const signIn = ({user, store, branch, token, manager, categories}) => {
      self.root.categoryStore.createOrUpdate(categories);
      self.root.managerStore.createOrUpdate(manager);
      if (branch) self.root.branchStore.createOrUpdate(branch);

      self.user = user;
      self.store = store;
      self.branch = branch ? branch.id : null;
      self.token = token;
      self.manager = manager.id;

      storage.set('user', user).then();
      storage.set('store', store).then();
      storage.set('branch', branch).then();
      storage.set('token', token).then();
      storage.set('manager', manager).then();
      storage.set('categories', categories).then();
      //storage.set('fcm_token', getSnapshot(self.store)).then();
    };

    const checkAuth = () => {
      if (!self.token || self.auth_checking) {
        return;
      }
      self.setValue('auth_checking', true);

      requester.get('/user/basic').then(response => {
        self.root.categoryStore.createOrUpdate(response.data.categories);
        self.root.managerStore.createOrUpdate(response.data.manager);
        if (response.data.branch) self.root.branchStore.createOrUpdate(response.data.branch);

        self.setValue('store', response.data.store);
        if (response.data.branch) self.setValue('branch', response.data.branch.id);
        self.setValue('manager', response.data.manager.id);
        self.setValue('user', response.data.user);
      }).catch(e => {
        console.log(e);
        self.signOut();
      }).finally(() => {
        self.setValue('auth_checking', false);
      });
    };

    return {
      setValue,
      signOut,
      signIn,
      checkAuth,
    };

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

    get authenticated() {
      return self.user !== null;
    },

  }));
