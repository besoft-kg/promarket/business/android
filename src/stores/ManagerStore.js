import {types as t} from 'mobx-state-tree';
import Manager from '../models/Manager';

export default t
  .model('ManagerStore', {

    items: t.optional(t.map(Manager), {}),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map(v => self.createOrUpdate(v));
        return;
      }

      self.items.set(item.id, {
        ...item,
      });
    };

    const clearItems = () => {
      self.items = {};
    };

    return {
      setValue,
      createOrUpdate,
      clearItems,
    };

  })
  .views(self => ({}));
