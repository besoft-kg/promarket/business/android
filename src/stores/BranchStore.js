import {getRoot, types as t} from 'mobx-state-tree';
import Branch from '../models/Branch';

export default t
  .model('BranchStore', {

    items: t.optional(t.map(Branch), {}),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map(v => self.createOrUpdate(v));
        return;
      }

      self.root.regionStore.createOrUpdate(item.region);

      self.items.set(item.id, {
        ...item,
        region: item.region_id
      });
    };

    const clearItems = () => {
      self.items = {};
    };

    return {
      setValue,
      createOrUpdate,
      clearItems,
    };

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

  }));
