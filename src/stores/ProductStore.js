import {getRoot, types as t} from 'mobx-state-tree';
import Product from '../models/Product';

export default t
  .model('ProductStore', {

    items: t.optional(t.map(Product), {}),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map(v => self.createOrUpdate(v));
        return;
      }

      self.root.colorStore.createOrUpdate(item.colors.map(v => ({
        ...v,
        category_id: item.category_id,
      })));

      self.items.set(item.id, {
        ...item,
        category: item.category_id,
        colors: item.colors.map(v => v.id),
      });
    };

    const clearItems = () => {
      self.items = {};
    };

    return {
      setValue,
      createOrUpdate,
      clearItems,
    };

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

  }));
