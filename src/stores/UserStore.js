import {types as t} from 'mobx-state-tree';
import User from '../models/User';

export default t
  .model('UserStore', {

    items: t.optional(t.map(User), {}),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map(v => self.createOrUpdate(v));
        return;
      }

      self.items.set(item.id, {
        ...item,
      });
    };

    const clearItems = () => {
      self.items = {};
    };

    return {
      setValue,
      createOrUpdate,
      clearItems,
    };

  })
  .views(self => ({}));
