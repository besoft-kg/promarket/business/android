import {types as t} from 'mobx-state-tree';

export default t.model('offer_branch', {
  offer_id: t.integer,
  branch_id: t.integer,
  color_id: t.maybeNull(t.integer),
});
