import {types as t} from "mobx-state-tree";

export default t
  .model('image', {
    id: t.identifierNumber,
    path: t.frozen({
        original: t.string,
    }),
    size: t.map(t.integer),
    dimensions: t.frozen({
        original: t.frozen({
            width: t.maybeNull(t.integer),
            height: t.maybeNull(t.integer),
        })
    }),
    extension: t.string,
  });
