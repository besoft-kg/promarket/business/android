import {getRoot, types as t} from 'mobx-state-tree';
import React from 'react';
import Image from './Image';

export default t
  .model('store', {

    id: t.identifierNumber,
    title: t.string,
    contacts: t.frozen(),
    about: t.maybeNull(t.string),
    logo: t.maybeNull(t.reference(Image)),

  }).views(self => ({

    get root() {
      return getRoot(self);
    },

  })).actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    return {
      setValue,
    };

  });
