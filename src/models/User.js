import {types as t} from "mobx-state-tree";

export default t
  .model('user', {

    id: t.identifierNumber,
    phone_number: t.string,
    full_name: t.string,

  }).actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    return {
      setValue,
    };

  }).views(self => ({



  }));
