import {types as t} from "mobx-state-tree";

export default t
  .model('manager', {

    id: t.identifierNumber,
    position: t.string,
    store_id: t.maybeNull(t.integer),
    branch_id: t.maybeNull(t.integer),
    user_id: t.maybeNull(t.integer),

  }).actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    return {
      setValue,
    };

  }).views(self => ({

    get is_branch() {
      return self.branch_id && self.store_id;
    },

    get is_store() {
      return !self.branch_id && self.store_id;
    },

  }));
