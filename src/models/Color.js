import {types as t} from 'mobx-state-tree';

export default t
  .model('color', {
    id: t.identifierNumber,
    title: t.string,
    hex_code: t.string,
    category_id: t.integer,
  });
