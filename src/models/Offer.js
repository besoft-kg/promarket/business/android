import {getRoot, types as t} from 'mobx-state-tree';
import Product from './Product';
import OfferBranch from './OfferBranch';
import moment from 'moment';

export default t
  .model('offer', {

    id: t.identifierNumber,
    price: t.integer,
    used: t.boolean,
    description: t.maybeNull(t.string),
    discount_percentage: t.maybeNull(t.integer),
    discount_start_on: t.maybeNull(t.Date),
    discount_end_on: t.maybeNull(t.Date),
    product_id: t.integer,
    store_id: t.integer,

    product: t.reference(Product),
    branches: t.array(OfferBranch),

    loading: t.optional(t.boolean, false),

  }).actions(self => {

    const setValue = (name, value) => self[name] = value;

    const toggleBranch = (color = null) => {
      if (color === null) {
        if (self.branches.find(g => g.offer_id === self.id)) {
          self.branches.splice(self.branches.indexOf(self.branches.find(v => v.offer_id === self.id)), 1);
        } else {
          self.branches.push({
            offer_id: self.id,
            branch_id: self.root.appStore.branch.id,
          });
        }
      } else {
        if (self.branches.find(g => g.color_id === color.id)) {
          self.branches.splice(self.branches.indexOf(self.branches.find(v => v.color_id === color.id)), 1);
        } else {
          self.branches.push({
            offer_id: self.id,
            branch_id: self.root.appStore.branch.id,
            color_id: color.id,
          });
        }
      }
    };

    return {
      setValue,
      toggleBranch,
    };

  }).views(self => ({

    get is_valid_data() {
      if (self.price <= 0) {
        return false;
      }
      return true;
    },

    get discount_active() {
      const today = new Date(new Date().toDateString());
      return self.discount_start_on && self.discount_end_on && self.discount_start_on <= today && self.discount_end_on >= today;
    },

    get sale_price() {
      if (self.discount_active) {
        return (self.price - ((self.price * self.discount_percentage) / 100).toFixed());
      }
      return self.price;
    },

    get ui_discount_start_on() {
      return moment(self.discount_start_on).format('DD-MM-YYYY');
    },

    get ui_discount_end_on() {
      return moment(self.discount_end_on).format('DD-MM-YYYY');
    },

    get root() {
      return getRoot(self);
    },

  }));
