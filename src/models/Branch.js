import {types as t} from "mobx-state-tree";
import float from './float';
import Region from './Region';

export default t
  .model('branch',{

    id: t.identifierNumber,
    title: t.maybeNull(t.string),
    region: t.reference(Region),
    address: t.string,
    address_lat: float,
    address_lng: float,
    contacts: t.maybeNull(t.array(t.frozen())),
    schedule: t.maybeNull(t.frozen()),

  });
