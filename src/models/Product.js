import {getRoot, types as t} from 'mobx-state-tree';
import React from 'react';
import Image from './Image';
import Category from './Category';
import Color from './Color';

export default t
  .model('product', {

    id: t.identifierNumber,
    title: t.string,
    description: t.maybeNull(t.string),
    parent_id: t.maybeNull(t.integer),
    category: t.maybeNull(t.reference(Category)),
    main_image: t.maybeNull(Image),
    colors: t.array(t.reference(Color)),

  }).views(self => ({

    get root() {
      return getRoot(self);
    },

  })).actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    return {
      setValue,
    };

  });
