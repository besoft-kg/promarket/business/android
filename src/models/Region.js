import {types as t} from 'mobx-state-tree';

export default t.model('region', {
  id: t.identifierNumber,
  title: t.string,
  parent_id: t.maybeNull(t.integer),
});
