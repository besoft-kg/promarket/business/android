import {action, observable} from "mobx";

class BaseModel {
  @observable id = null;

  constructor(props) {
    if (props) {
      Object.keys(props).map((v) => {
        if (v in this) {
          this[v] = props[v];
        }
      });
    }
  }

  @action _update(props) {
    Object.keys(props).map((v) => {
      if (v in this) {
        this[v] = props[v];
      }
    });
  }

  @action _setValue = (name, value) => {
    if (name in this) this[name] = value;
  };
}

export default BaseModel;
