import {types as t} from 'mobx-state-tree';
import Image from './Image';

export default t
  .model('category', {
    id: t.identifierNumber,
    title: t.string,
    short_title: t.string,
    icon: t.maybeNull(Image),
    all_subcategories: t.array(t.integer),
    colorable: t.boolean,
    parent_id: t.integer,
  });
