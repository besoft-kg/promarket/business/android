import React from 'react';
import BaseComponent from '../components/BaseComponent';
import {inject, observer} from 'mobx-react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';
import {APP_PRIMARY_COLOR} from '../utils/settings';
import AddOfferScreen from '../screens/AddOfferScreen';
import EditOfferScreen from '../screens/EditOfferScreen';
import OffersScreen from '../screens/OffersScreen';
import BranchViewScreen from '../screens/BranchViewScreen';
import {HeaderButton, HeaderButtons, HiddenItem, OverflowMenu} from 'react-navigation-header-buttons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AboutBranchScreen from '../screens/AboutBranchScreen';
import {Alert} from 'react-native';

const Stack = createStackNavigator();

@inject('store') @observer
class HomeStack extends BaseComponent {

  quite = () => {
    Alert.alert(
      'Выйти',
      'Вы уверены что хотите выйти?',
      [
        {text: 'Отмена', onPress: () => console.log('Cancel Pressed')},
        {text: 'Выход', onPress: () => this.appStore.signOut()},
      ],
      { cancelable: true }
    );
  };

  render() {
    console.log(this.appStore);
    return (
      <>
        <Stack.Navigator
          screenOptions={{
            headerStyle: {
              backgroundColor: APP_PRIMARY_COLOR,
            },
            headerTintColor: '#fff',
          }}
          initialRouteName={'HomeScreen'}
        >
          <Stack.Screen
            name={'HomeScreen'}
            component={HomeScreen}
            options={{
              title: this.appStore.manager ? (this.appStore.manager.is_branch ? `Филиал ${this.appStore.store.title}` : `Магазин ${this.appStore.store.title}`) : 'ProMarket Business',
              headerRight: () => (
                <>
                  {this.appStore.manager && (
                    <HeaderButtons
                      HeaderButtonComponent={props => (
                        <HeaderButton IconComponent={MaterialIcons} iconSize={23} color="#fff" {...props} />
                      )}>
                      {/*<Item title={'plus'} onPress={() => alert('a')} iconName={'add'}/>*/}
                      <OverflowMenu OverflowIcon={<MaterialIcons name="more-vert" size={23} color="#fff"/>}>

                         {/*<HiddenItem onPress={() => this.props.navigation.navigate('AddOfferScreen')}
                                      title={'Добавить товар'}/>*/}
                          {!this.appStore.manager.is_store && (
                          <HiddenItem onPress={() => this.props.navigation.navigate('AboutBranchScreen')}
                                      title={'О филиале'}/>
                        )}


                        <HiddenItem onPress={() => this.quite()} title={'Выйти'}/>
                      </OverflowMenu>
                    </HeaderButtons>
                  )}
                </>
              ),
            }}
          />

          <Stack.Screen
            name={'AddOfferScreen'}
            options={{
              title: 'Добавить товар',
            }}
            component={AddOfferScreen}
          />

          <Stack.Screen
            name={'OffersScreen'}
            options={{
              title: 'Цены',
            }}
            component={OffersScreen}
          />

          <Stack.Screen
            name={'EditOfferScreen'}
            component={EditOfferScreen}
            options={{
              title: 'Редактировать товар',
            }}
          />

          <Stack.Screen
            name={'BranchViewScreen'}
            component={BranchViewScreen}
            options={{
              title: 'Филиал',
            }}
          />

          <Stack.Screen
            name={'AboutBranchScreen'}
            component={AboutBranchScreen}
            options={{
              title: 'О филиале',
            }}
          />

        </Stack.Navigator>
      </>
    );
  }
}

export default HomeStack;
