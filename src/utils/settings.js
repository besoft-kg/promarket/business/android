import axios from 'axios';
import {buildFormData} from "./index";
import storage from './storage';

export const APP_NAME = 'ProMarket Business';
export const APP_PRIMARY_COLOR = '#009846';
export const APP_SECONDARY_COLOR = '#ef7f1a';

export const DEBUG_MODE = process.env.NODE_ENV === "production";
export const NO_AVATAR_URL = '/images/no-avatar-300x300.png';
export const WEB_URL = DEBUG_MODE ? "http://localhost:3000" : "https://business.promarket.besoft.kg";
export const API_VERSION = 1;
export const APP_VERSION = '0.0.7';
export const APP_VERSION_CODE = 7;
export const API_URL = DEBUG_MODE ? 'http://192.168.0.104:5002' : 'https://api.business.promarket.besoft.kg';
export const OFFICE_API_URL = 'https://api.office.promarket.besoft.kg';
// export const OFFICE_API_URL = DEBUG_MODE ? 'http://192.168.43.180:5000' : 'https://api.office.promarket.besoft.kg';
export const API_URL_WITH_VERSION = API_URL + '/v' + API_VERSION;

const axios_instance = axios.create({
  baseURL: API_URL_WITH_VERSION,
  responseType: 'json',
  responseEncoding: 'utf8',
});

axios_instance._configs = {
  silence: false,
};

export const requester = {
  get: async (url, params = {}, silence = false) => {
    axios_instance._configs.silence = silence;

    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    };

    const token = await storage.get('token');
    if (token) {
      headers['Authorization'] = `Bearer ${token}`;
    }

    return axios_instance.get(`${API_URL_WITH_VERSION}${url}`, {
      params,
      headers,
    });
  },
  post: async (url, data, silence = false) => {
    axios_instance._configs.silence = silence;
    const form_data = new FormData();
    if (data) buildFormData(form_data, data);

    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
    };

    const token = await storage.get('token');
    if (token) {
      headers['Authorization'] = `Bearer ${token}`;
    }

    return axios_instance.post(`${API_URL_WITH_VERSION}${url}`, form_data, {headers});
  },
  put: function (url, data, silence = false) {
    axios_instance._configs.silence = silence;
    return axios_instance.put(`${API_URL_WITH_VERSION}${url}`, data);
  },
  delete: async (url, data, silence = false) => {
    axios_instance._configs.silence = silence;

    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    };

    const token = await storage.get('token');
    if (token) {
      headers['Authorization'] = `Bearer ${token}`;
    }

    return axios_instance.request({
      url: `${API_URL_WITH_VERSION}${url}`,
      method: 'delete',
      data,
      headers,
    });
  }
};

export function initialize(appStore){

  axios_instance.interceptors.request.use(function(config){
    if (appStore.token) config.headers.common.Authorization = 'Bearer ' + appStore.token;
    return config;
  }, function (error){
    if (!axios_instance._configs.silence) appStore.showError(error.message || error);
    if (error instanceof CustomError) throw error;
    //throw new CustomError().setError(error);
  });

  axios_instance.interceptors.response.use(function (response){
    if (response.data.status < 0){
      switch (response.data.result) {
        case 'token_is_expired':
          appStore.showInfo('Срок действия вашего токена истек, пожалуйста, войдите снова!');
          appStore.signOut();
          break;
        case 'token_is_invalid':
          appStore.showInfo('Ваш токен недействителен, пожалуйста, войдите снова!');
          appStore.signOut();
          break;
      }

      if (!axios_instance._configs.silence) {
        let message = response.data.payload;
        switch (response.data.result) {
          case 'invalid_params':
            message = 'Неверные параметры!';
            break;

          default:
            break;
        }
        appStore.showError(message);
      }

      throw new CustomError(response);
    }
    return response;
  }, function (error){
    //if (!(error instanceof CustomError)) error = new CustomError().setError(error);
    if (!axios_instance._configs.silence) appStore.showError(error.description || error.message);
    return Promise.reject(error);
  });
}
