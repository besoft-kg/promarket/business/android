import {Dimensions} from 'react-native';

export const WINDOW_SIZE = Dimensions.get('window');

export const buildFormData = (formData, data, parentKey) => {
  if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
    Object.keys(data).forEach(key => {
      buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
    });
  } else {
    let value = data == null ? '' : data;
    if (value === true) value = "1";
    else if (value === false) value = "0";
    formData.append(parentKey, value);
  }
};
