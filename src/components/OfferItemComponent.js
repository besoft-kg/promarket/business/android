import BaseComponent from './BaseComponent';
import {Card, Text} from 'react-native-elements';
import {TouchableOpacity, View} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/core';

class OfferItemComponent extends BaseComponent {
  render() {
    const {item} = this.props;
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('EditOfferScreen', {id: item.id})}>
        <Card containerStyle={{margin: 8}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            {item.used && <Text style={{backgroundColor: '#26418f', color: 'white', marginRight: 3, fontSize: 12, borderRadius: 6, padding: 3}}>Б/У</Text>}
            <Text style={{fontSize: 15}}>{item.product.title}</Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-start'}}>
            <Text style={{fontSize: 20}}>
              <Text>{item.discount_active ? item.sale_price : item.price}{' '}</Text>
              <Text style={{textDecorationLine: 'underline'}}>с</Text>
            </Text>
            {item.discount_active ? (
              <Text style={{
                fontSize: 16,
                marginLeft: 8,
                textDecorationLine: 'line-through',
                textDecorationStyle: 'solid',
                color: '#808080',
              }}>
                <Text>{item.price}{' '}</Text>
                <Text style={{textDecorationLine: 'underline'}}>с</Text>
              </Text>
            ) : null}
          </View>

          {item.discount_active ? (
            <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
              <View flexDirection={'row'}>
                <Text>
                  {`с ${item.ui_discount_start_on}`}
                </Text>
                <Text style={{marginLeft: 8}}>
                  {`до ${item.ui_discount_end_on}`}
                </Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center', backgroundColor: '#ff4700', borderRadius: 5}}>
                <Text style={{color: 'white', marginHorizontal: 6}}>{item.discount_percentage}%</Text>
              </View>
            </View>
          ) : null}
        </Card>
      </TouchableOpacity>
    );
  }
}

export default function(props) {
  const navigation = useNavigation();

  return <OfferItemComponent {...props} navigation={navigation} />;
};
