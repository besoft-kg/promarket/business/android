import React from  'react';
import {action} from 'mobx';

class BaseComponent extends React.Component{
  constructor(props) {
    super(props);

    if ('store' in this.props) {
      this.store = this.props.store;
      this.appStore = this.store.appStore;
      this.categoryStore = this.store.categoryStore;
      this.userStore = this.store.userStore;
      this.branchStore = this.store.branchStore;
      this.managerStore = this.store.managerStore;
      this.offerStore = this.store.offerStore;
      this.productStore = this.store.productStore;
      this.regionStore = this.store.regionStore;
      this.colorStore = this.store.colorStore;
    }
  }

  @action setValue = (name, value) => this[name] = value;
}

export default BaseComponent;
