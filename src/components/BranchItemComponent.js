import React from 'react';
import BaseComponent from './BaseComponent';
import {TouchableOpacity, View} from 'react-native';
import {Card, Text} from 'react-native-elements';
import moment from 'moment';
import {useNavigation} from '@react-navigation/core';

const day_name = moment().format('ddd').toLowerCase();

class BranchItemComponent extends BaseComponent {
  render() {
    const {item} = this.props;
    console.log(item)
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('BranchViewScreen', {id: item.id})}>
        <Card containerStyle={{margin: 8}}>
          {item.title && (
            <View>
              <Text style={{fontSize: 18}}>Название филиала</Text>
            </View>
          )}
          <View>
            <Text>Регион: {item.region.title}</Text>
          </View>
          <View>
            <Text>{`Адрес: ${item.address}`}</Text>
          </View>
          <View>
            {item.schedule && day_name in item.schedule &&
            <Text>
              {`Время работы: ${item.schedule[day_name].open} - ${item.schedule[day_name].close}`}
            </Text>}
          </View>
        </Card>
      </TouchableOpacity>
    );
  }
}

export default function (props) {
  const navigation = useNavigation();

  return <BranchItemComponent {...props} navigation={navigation}/>;
};
