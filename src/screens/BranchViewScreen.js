import React from 'react';
import BaseScreen from './BaseScreen';
import { Card, ListItem, Text } from "react-native-elements";
import {inject, observer} from 'mobx-react';
import {observable} from 'mobx';
import { Linking, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/dist/MaterialCommunityIcons";
import { APP_PRIMARY_COLOR } from "../utils/settings";

const days = ['Понедельник','Вторник','Среда','Четверг','Пятница','Суббота','Воскресенье']

@inject('store') @observer
class BranchViewScreen extends BaseScreen {
  @observable item = null;

  constructor(props) {
    super(props);
    this.item = this.branchStore.items.get(this.props.route.params.id);
  }

  contacts = (v) => {
    switch (v.type) {
      case "phone":
        Linking.openURL(`tel:+${v.value}`).then(r => {
        });
        break;
      case "whatsapp":
        Linking.openURL(`whatsapp://send?text=hello&phone=+${v.value}`).then(r => {
        });
        break;
      case "instagram":
        Linking.openURL(`https://www.instagram.com/${v.value}`).then(r => {
        });
        break;
    }
  };

  render() {
    console.log(this.item)
    return (
      <>
        <Card containerStyle={{ margin: 8, padding: 8 }}>
          <Text style={{ fontWeight: "bold", fontSize: 20, alignItems: "center" }}>Адресс</Text>
          {this.item && (
            <>
              {this.item.title &&
              <Text>{this.item.title}</Text>
              }
              {this.item.address &&
              <Text style={{ fontSize: 15, color: "#1C2833" }}>{this.item.address}</Text>
              }
            </>
          )}
        </Card>

        <Card containerStyle={{ margin: 8, padding: 8 }}>
          <Text style={{ fontWeight: "bold", fontSize: 20, alignItems: "center" }}>Контакты</Text>
          {this.item.contacts && this.item.contacts.map((v) =>
            <ListItem bottomDivider>
              <ListItem.Content>
                <TouchableOpacity style={{ flexDirection: "row", justifyContent: "flex-start" }}
                                  onPress={() => this.contacts(v)}>
                  <Icon color={APP_PRIMARY_COLOR} size={25} name={v.type} />
                  <Text
                    style={{ color: "#0096c7" }}>  {v.type === "whatsapp" && "+"}{v.type === "phone" && "+"}{v.value}</Text>
                </TouchableOpacity>
              </ListItem.Content>
            </ListItem>
          )}
        </Card>

        {this.item.schedule && (
          <Card containerStyle={{ margin: 8, padding: 8 }}>
            <>
              <Text style={{ fontWeight: "bold", fontSize: 20, alignItems: "center" }}>{"Время работы"}</Text>
              {this.item.schedule && Object.keys(this.item.schedule).map((i, g) => (
                <ListItem key={g} bottomDivider>
                  <ListItem.Content style={{ flex: 1, flexDirection: "row" }} justifyContent={"space-between"}>
                    <Text>{days[g]}</Text>
                    {this.item.schedule[i].open && this.item.schedule[i].close ? (
                      <Text>{this.item.schedule[i].open} - {this.item.schedule[i].close}</Text>) : (
                      <Text style={{ color: "red" }}>Не работает</Text>
                    )}
                  </ListItem.Content>
                </ListItem>
              ))}
            </>
          </Card>
        )}
      </>
    );
  }
}

export default BranchViewScreen;
