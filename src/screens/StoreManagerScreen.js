import React from 'react';
import BaseScreen from './BaseScreen';
import {FlatList} from 'react-native';
import {values} from 'mobx';
import {inject, observer} from 'mobx-react';
import {onSnapshot} from 'mobx-state-tree';
import OfferItemComponent from '../components/OfferItemComponent';
import BranchItemComponent from '../components/BranchItemComponent';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

const Tab = createMaterialTopTabNavigator();

@inject('store') @observer
class StoreManagerScreen extends BaseScreen {
  componentDidMount() {
    onSnapshot(this.offerStore.items, () => this.forceUpdate());
  }

  render() {
    return (
      <>
        <Tab.Navigator backBehavior={'none'}>
          <Tab.Screen name="Offers" options={{title: 'Товары'}}>
            {(props) => (
              <FlatList
                data={values(this.offerStore.items).slice()}
                renderItem={({item}) => (
                  <OfferItemComponent item={item}/>
                )}
                {...props}
              />
            )}
          </Tab.Screen>
          <Tab.Screen name="Branches" options={{title: 'Филиалы'}}>
            {(props) => (
              <FlatList
                data={values(this.branchStore.items).slice()}
                renderItem={({item}) => (
                  <BranchItemComponent item={item}/>
                )}
                {...props}
              />
            )}
          </Tab.Screen>
        </Tab.Navigator>
        </>
    );
  }
}

export default StoreManagerScreen;
