import React from 'react';
import BaseComponent from '../components/BaseComponent';
import {observer, Observer} from 'mobx-react';
import {action, observable} from 'mobx';
import {requester} from '../utils/settings';
import {Button, Input, ListItem} from 'react-native-elements';
import {FlatList} from 'react-native';

@observer
class AddOfferScreen extends BaseComponent {
  @observable item = [];
  @observable query = '';
  @observable loading = false;
  @observable selected_products = [];

  search = () => {
    if (this.loading) {
      return;
    }
    this.setValue('loading', true);
    requester.get('/product/search', {
      query: this.query,
    }).then((response) => {
      this.setValue('item', response.data);
    }).catch(e => {
      console.log(e.response.data);
    }).finally(() => {
      this.setValue('loading', false);
    });
  };

  keyExtractor = (item, index) => index.toString();

  @action toggleSelectedProduct = (item) => {
    const selected = this.selected_products.find(v => v === item);
    if (selected) {
      this.selected_products.splice(this.selected_products.indexOf(item), 1);
    } else {
      this.selected_products.push(item);
    }
  };

  renderItem = ({item}) => (
    <Observer>
      {() => {
        const selected = this.selected_products.find(v => v === item);
        return (
          <ListItem bottomDivider onPress={() => this.toggleSelectedProduct(item)}>
            <ListItem.Content>
              <ListItem.Title>{item.title}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Chevron color={selected ? 'red' : 'green'} name={selected ? 'remove' : 'add'}/>
          </ListItem>
        );
      }}
    </Observer>
  );

  goToOffers = () => this.props.navigation.navigate('OffersScreen', {items: this.selected_products});

  render() {
    return (
      <>
        <FlatList
          ListHeaderComponent={(
            <Input
              onChangeText={(e) => this.setValue('query', e)}
              placeholder={'Что добавить?'}
              disabled={this.loading}
              autoFocus={true}
              blurOnSubmit={true}
              onSubmitEditing={this.search}
              returnKeyType={'search'}
              rightIcon={{
                name: 'search',
                onPress: this.search,
              }}store
            />
          )}
          keyExtractor={this.keyExtractor}
          data={this.item}
          renderItem={this.renderItem}
          ListFooterComponent={(
            <>
              {this.selected_products.length > 0 && (
                <Button onPress={() => this.goToOffers()} containerStyle={{margin: 8}} title={`Указать цены (${this.selected_products.length})`} />
              )}
            </>
          )}
        />
      </>
    );
  }
}

export default AddOfferScreen;
