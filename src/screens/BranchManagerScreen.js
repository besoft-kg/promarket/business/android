import React from 'react';
import BaseScreen from './BaseScreen';
import {Card, Overlay, Text, Button} from 'react-native-elements';
import {ActivityIndicator, FlatList, Image, TouchableOpacity, View} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import {comparer, computed, observable, values} from 'mobx';
import {APP_PRIMARY_COLOR, OFFICE_API_URL, requester} from '../utils/settings';
import {inject, observer} from 'mobx-react';
import {applySnapshot, getSnapshot, onSnapshot} from 'mobx-state-tree';

@inject('store') @observer
class BranchManagerScreen extends BaseScreen {
  @observable active_offer = null;
  @observable loading = false;
  draft_snapshot = null;

  toggleAvailability = (item) => {
    if (item.loading) {
      return;
    }
    item.setValue('loading', true);
    requester.post('/offer/branch', {
      offer_id: item.id,
    }).then(res => {
      console.log(res.data);
      item.toggleBranch();
    }).catch(e => {
      console.log(e);
    }).finally(() => {
      this.setValue('active_offer', null);
      item.setValue('loading', false);
    });
  };

  componentDidMount() {
    onSnapshot(this.offerStore.items, () => this.forceUpdate());
  }

  openModal = (item) => {
    this.draft_snapshot = {
      ...getSnapshot(item),
      product: null,
    };
    this.setValue('active_offer', item);
  };

  saveAvailability = () => {
    if (this.active_offer === null || this.loading) {
      return;
    }
    this.setValue('loading', true);
    requester.post('/offer/branch', {
      offer_id: this.active_offer.id,
      colors: getSnapshot(this.active_offer.branches),
    }).then(res => {
      this.draft_snapshot = {
        ...getSnapshot(this.active_offer),
        product: null
      };
      this.offerStore.createOrUpdate(this.draft_snapshot);
    }).catch(e => {
      alert(e.message);
      applySnapshot(this.active_offer, this.draft_snapshot);
    }).finally(() => {
      this.setValue('loading', false);
      this.setValue('active_offer', null);
    });
  };

  @computed get has_changes() {
    return this.active_offer && !comparer.structural({
      ...getSnapshot(this.active_offer),
      product: null,
    }, this.draft_snapshot);
  }

  render() {
    return (
      <>
        <Overlay
          transparent={true}
          isVisible={this.active_offer !== null}
          onBackdropPress={() => this.has_changes === false ? this.setValue('active_offer', null) : null}
        >
          {this.active_offer !== null && (
            <View style={{padding: 10}}>
              <Text style={{marginBottom: 5}}>{this.active_offer.product.title}</Text>
              {this.active_offer.product.colors && this.active_offer.product.colors.map(v =>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <CheckBox
                    value={this.active_offer.branches.find(g => g.color_id === v.id) !== undefined || this.active_offer.branches.find(g => g.color_id === null) !== undefined}
                    onValueChange={() => this.active_offer.toggleBranch(v)}
                    title={`${v.title}`}
                  />
                  <Text onPress={() => this.active_offer.toggleBranch(v)}>
                    {v.title}
                  </Text>
                </View>
              )}
              <Button
                title={'Сохранить'}
                disabled={this.loading || !this.has_changes}
                loading={this.loading}
                onPress={() => this.saveAvailability()}
                style={{alignSelf: 'flex-end'}}
              />
            </View>
          )}
        </Overlay>

        <FlatList
          data={values(this.offerStore.items).slice()}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => item.product.category.colorable && item.product.colors.length > 0 ? this.openModal(item) : this.toggleAvailability(item)}>
              <Card containerStyle={{margin: 8, padding: 8}}>
                <View flexDirection={'row'}>
                  <View alignSelf={'center'} style={{padding: 3, marginRight: 8}}>
                    {item.product.main_image !== null ? (
                      <Image style={{width: 50, height: 50}} resizeMode={'contain'}
                             source={{uri: `${OFFICE_API_URL}/${item.product.main_image.path.original}`}}/>
                    ) : (
                      <Image style={{backgroundColor: '#fff', width: 50, height: 50}} resizeMode={'contain'}
                             source={require('../assets/images/logo-opacity-70-320-354.png')}/>
                    )}
                  </View>
                  <View flex={1}>
                    <View>
                      <Text style={{fontSize: 15}}>{item.product.title}</Text>
                    </View>
                    <View style={{flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-start'}}>
                      <Text style={{fontSize: 20}}>
                        <Text>{item.discount_active ? item.sale_price : item.price}{' '}</Text>
                        <Text style={{textDecorationLine: 'underline'}}>с</Text>
                      </Text>
                      {item.discount_active ? (
                        <Text style={{
                          fontSize: 16,
                          marginLeft: 8,
                          textDecorationLine: 'line-through',
                          textDecorationStyle: 'solid',
                          color: '#808080',
                        }}>
                          <Text>{item.price}{' '}</Text>
                          <Text style={{textDecorationLine: 'underline'}}>с</Text>
                        </Text>
                      ) : null}
                    </View>

                    {item.discount_active ? (
                      <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
                        <View flexDirection={'row'}>
                          <Text>
                            {`с ${item.ui_discount_start_on}`}
                          </Text>
                          <Text style={{marginLeft: 8}}>
                            {`до ${item.ui_discount_end_on}`}
                          </Text>
                        </View>
                        <View style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          backgroundColor: '#ff4700',
                          borderRadius: 5,
                        }}>
                          <Text style={{color: 'white', marginHorizontal: 6}}>{item.discount_percentage}%</Text>
                        </View>
                      </View>
                    ) : null}
                    {item.loading ? (
                      <View style={{alignItems: 'flex-start', justifyContent: 'center'}}>
                        <ActivityIndicator color={APP_PRIMARY_COLOR}/>
                      </View>
                    ) : (
                      <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        {item.branches.length > 0 ? (
                          <Text style={{color: 'green'}}>Есть в наличии</Text>
                        ) : (
                          <Text style={{color: 'red'}}>Нет в наличии</Text>
                        )}
                        {item.product.category.colorable && item.product.colors.length > 0 && (
                          <View style={{marginLeft: 5, flexDirection: 'row', alignItems: 'center'}}>
                            {item.branches.length === item.product.colors.length ? (
                              <Text>Все цвета</Text>
                            ) : item.branches.map(b => (
                              <View style={{
                                width: 16,
                                marginLeft: 3,
                                borderColor: 'grey',
                                borderWidth: 1,
                                borderRadius: 10,
                                height: 16,
                                backgroundColor: `#${this.colorStore.items.get(b.color_id).hex_code}`,
                              }}/>
                            ))}
                          </View>
                        )}
                      </View>
                    )}
                  </View>
                </View>
              </Card>
            </TouchableOpacity>
          )}
        />
      </>
    );
  }
}

export default BranchManagerScreen;
