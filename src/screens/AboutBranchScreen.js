import React from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { Card, ListItem } from "react-native-elements";
import { inject, observer } from "mobx-react";
import BaseScreen from "./BaseScreen";
import Icon from "react-native-vector-icons/dist/MaterialCommunityIcons";
import { Linking } from "react-native";
import { APP_PRIMARY_COLOR } from "../utils/settings";

const days = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"];

@inject("store") @observer
class AboutBranchScreen extends BaseScreen {

  contacts = (v) => {
    switch (v.type) {
      case "phone":
        Linking.openURL(`tel:+${v.value}`).then(r => {
        });
        break;
      case "whatsapp":
        Linking.openURL(`whatsapp://send?text=hello&phone=+${v.value}`).then(r => {
        });
        break;
      case "instagram":
        Linking.openURL(`https://www.instagram.com/${v.value}`).then(r => {
        });
        break;
    }
  };

  render() {
    return (
      <>
        <Card containerStyle={{ margin: 8, padding: 8 }}>
          <Text style={{ fontWeight: "bold", fontSize: 20, alignItems: "center" }}>Адресс</Text>
          {this.appStore && (
            <>
              {this.appStore.branch.title &&
              <Text>{this.appStore.branch.title}</Text>
              }
              {this.appStore.branch.address &&
              <Text style={{ fontSize: 15, color: "#1C2833" }}>{this.appStore.branch.address}</Text>
              }
            </>
          )}
        </Card>

        <Card containerStyle={{ margin: 8, padding: 8 }}>
          <Text style={{ fontWeight: "bold", fontSize: 20, alignItems: "center" }}>Контакты</Text>
          {this.appStore.branch.contacts && this.appStore.branch.contacts.map((v) =>
            <ListItem bottomDivider>
              <ListItem.Content>
                <TouchableOpacity style={{ flexDirection: "row", justifyContent: "flex-start" }}
                                  onPress={() => this.contacts(v)}>
                  <Icon color={APP_PRIMARY_COLOR} size={25} name={v.type} />
                  <Text
                    style={{ color: "#0096c7" }}>  {v.type === "whatsapp" && "+"}{v.type === "phone" && "+"}{v.value}</Text>
                </TouchableOpacity>
              </ListItem.Content>
            </ListItem>
          )}
        </Card>

        {this.appStore.branch.schedule && (
          <Card containerStyle={{ margin: 8, padding: 8 }}>
            <>
              <Text style={{ fontWeight: "bold", fontSize: 20, alignItems: "center" }}>{"Время работы"}</Text>
              {this.appStore.branch.schedule && Object.keys(this.appStore.branch.schedule).map((i, g) => (
                <ListItem key={g} bottomDivider>
                  <ListItem.Content style={{ flex: 1, flexDirection: "row" }} justifyContent={"space-between"}>
                    <Text>{days[g]}</Text>
                    {this.appStore.branch.schedule[i].open && this.appStore.branch.schedule[i].close ? (
                      <Text>{this.appStore.branch.schedule[i].open} - {this.appStore.branch.schedule[i].close}</Text>) : (
                      <Text style={{ color: "red" }}>Не работает</Text>
                    )}
                  </ListItem.Content>
                </ListItem>
              ))}
            </>
          </Card>
        )}
      </>
    );
  }
}

export default AboutBranchScreen;
