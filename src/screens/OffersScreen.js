import React from 'react';
import BaseComponent from '../components/BaseComponent';
import {observer, Observer} from 'mobx-react';
import {Button, ListItem, Text} from 'react-native-elements';
import {FlatList, View} from 'react-native';
import {observable} from 'mobx';
import {requester} from '../utils/settings';

@observer
class OffersScreen extends BaseComponent {
  @observable items = [];
  @observable loading = false;

  constructor(props) {
    super(props);

    this.items = this.props.route.params.items.map(v => ({
      ...v,
      price: null,
      used: false,
    }));
  }

  keyExtractor = (item, index) => index.toString();

  setPrice = (item, price) => {
    this.items[this.items.indexOf(item)].price = price;
  };

  setUsed = (item, s) => {
    this.items[this.items.indexOf(item)].used = s;
  };

  renderItem = ({item}) => (
    <Observer>
      {() => {
        return (
          <ListItem bottomDivider>
            <ListItem.Content>
              <ListItem.Title>{item.title}</ListItem.Title>
              <View flex={1} flexDirection={'row'} alignItems={'center'}>
                <ListItem.Input value={item.price} onChangeText={e => this.setPrice(item, e)} inputStyle={{textAlign: 'left'}} keyboardType={'numeric'} placeholder={'Цена товара'} />
                <ListItem.CheckBox
                  checked={item.used}
                  onPress={() => this.setUsed(item, !item.used)}
                  iconType={'material'}
                  checkedIcon={'done'}
                  uncheckedIcon={'done'}
                />
                <Text onPress={() => this.setUsed(item, !item.used)} style={{marginLeft: 8, color: '#808080'}}>Б/У</Text>
              </View>
            </ListItem.Content>
          </ListItem>
        );
      }}
    </Observer>
  );

  parseInt = (v) => {
    const i = parseInt(v);
    return isNaN(i) ? 0 : i;
  };

  save = () => {
    if (this.loading) return;
    this.setValue('loading', true);
    requester.post('/offer', {
      items: this.items.map(v => ({
        product_id: v.id,
        used: v.used,
        price: v.price,
      })),
    }).then(response => {
      console.log(response);
      this.props.navigation.navigate('HomeScreen')
    }).catch(e => {
      console.log(e.response.data);
    }).finally(() => {
      this.setValue('loading', false);
    });
  };

  render() {
    return (
      <>
        <FlatList
          keyExtractor={this.keyExtractor}
          data={this.items}
          renderItem={this.renderItem}
          ListFooterComponent={(
            <Button
              loading={this.loading}
              onPress={() => this.save()}
              containerStyle={{margin: 8}}
              disabled={this.items.filter(v => this.parseInt(v.price) <= 0).length > 0 || this.loading}
              title={`Сохранить товары`}
            />
          )}
        />
      </>
    );
  }
};

export default OffersScreen;
