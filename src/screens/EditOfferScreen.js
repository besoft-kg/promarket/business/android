import React from 'react';
import BaseComponent from '../components/BaseComponent';
import {inject, observer} from 'mobx-react';
import {Card, CheckBox, Input, Text, Button} from 'react-native-elements';
import {APP_PRIMARY_COLOR, requester} from '../utils/settings';
import {comparer, computed, observable} from 'mobx';
import {ActivityIndicator, TouchableOpacity, View, Alert} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import {clone, getSnapshot} from 'mobx-state-tree';
import {HeaderButton, HeaderButtons, Item} from 'react-navigation-header-buttons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

@inject('store') @observer
class EditOfferScreen extends BaseComponent {
  @observable loading = false;
  @observable saving = false;

  @observable date_picker_start = false;
  @observable date_picker_end = false;
  @observable item = null;

  discount_start_ref = React.createRef();
  discount_end_ref = React.createRef();

  constructor(props) {
    super(props);

    this.props.navigation.setOptions({
      headerRight: () => (
        <>
          <HeaderButtons
            HeaderButtonComponent={props => (
              <HeaderButton IconComponent={MaterialIcons} iconSize={23} color="#fff" {...props} />
            )}>
            <Item title={'Удалить'} onPress={() => this.delete()} iconName={'delete'}/>
          </HeaderButtons>
        </>
      ),
    });
  }

  fetchItem = () => {
    if (this.loading) {
      return;
    }
    this.setValue('loading', true);
    requester.get('/offer', {id: this.props.route.params.id}).then(res => {
      this.offerStore.createOrUpdate(res.data);
      this.item = clone(this.offerStore.items.get(this.props.route.params.id));
    }).catch(e => {
      console.log(e);
    }).finally(() => {
      this.setValue('loading', false);
    });
  };

  delete = () => {
    Alert.alert(
      'Удалить товар из магазина?',
      'Вы уверены что хотите удалить товар из вашего магазина?',
      [
        {
          text: 'Удалить', onPress: () => {
            requester.delete('/offer', {
              id: this.props.route.params.id,
            }).then((response) => {
              this.props.navigation.goBack();
              setTimeout(() => this.offerStore.remove(this.props.route.params.id), 100);
            }).catch(e => {
              console.log(e.response);
            });
          },
        },
        {text: 'Отмена', onPress: () => console.log('отмена')},
      ], {cancelable: false},
    );
  };

  componentDidMount() {
    this.fetchItem();
  }

  save = () => {
    if (this.saving) {
      return;
    }

    requester.post('/offer', {
      product_id: this.item.product_id,
      price: this.item.price,
      used: this.item.used,
      description: this.item.description,
      discount_percentage: this.item.discount_percentage,
      start_discount: moment(this.item.discount_start_on).format('YYYY-MM-DD'),
      end_discount: moment(this.item.discount_end_on).format('YYYY-MM-DD'),
    }).then((response) => {
      this.offerStore.createOrUpdate(response.data);
      this.setValue('item', clone(this.offerStore.items.get(response.data.id)));
      this.props.navigation.goBack();
    }).catch(e => {
      console.log(e);
    });
  };

  setDiscountPercentage = (value) => {
    if (isNaN(value)) {
      return;
    }
    this.item.setValue('discount_percentage', value >= 100 ? 99 : value);
  };

  setDate = (name, e) => {
    const current_date = e || this.item[name];
    this.item.setValue(name, current_date);

    this.setValue('date_picker_start', false);
    this.setValue('date_picker_end', false);
    this.discount_start_ref.blur();

    if (name === 'discount_start_on') {
      if (new Date(this.item.discount_start_on) > new Date(this.item.discount_end_on)) {
        this.item.setValue('discount_end_on', current_date);
      }
    }
  };

  @computed get has_changes() {
    return !comparer.structural(getSnapshot(this.item), getSnapshot(this.offerStore.items.get(this.props.route.params.id)));
  }

  render() {
    if (!this.item || this.loading) {
      return (
        <View alignItems={'center'} flex={1} justifyContent={'center'}>
          <ActivityIndicator color={APP_PRIMARY_COLOR} size={34}/>
        </View>
      );
    }

    return (
      <View style={{padding: 8}}>
        <Card containerStyle={{margin: 0, marginBottom: 8}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Input
              containerStyle={{width: '80%'}}
              value={`${this.item.price}`}
              keyboardType={'numeric'}
              leftIcon={{type: 'material', name: 'attach-money'}}
              onChangeText={value => this.item.setValue('price', +value)}
            />

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <CheckBox
                containerStyle={{margin: 0, padding: 0}}
                checked={this.item.used}
                onPress={() => this.item.setValue('used', !this.item.used)}
                iconType={'material'}
                checkedIcon={'check-circle'}
                uncheckedIcon={'check-circle-outline'}
              />
              <Text>Б/У</Text>
            </View>

          </View>
          <Input
            multiline={true}
            placeholder="Описание товара"
            value={this.item.description}
            leftIcon={{type: 'material', name: 'description'}}
            onChangeText={value => this.item.setValue('description', value)}
          />
          <Input
            keyboardType={'numeric'}
            placeholder="Скидка (%)"
            value={this.item.discount_percentage ? `${this.item.discount_percentage}` : ''}
            leftIcon={{type: 'material-community', name: 'label-percent'}}
            onChangeText={value => this.setDiscountPercentage(+value)}
          />

          {this.item.discount_percentage > 0 && (
            <>
              <Text>Цена со
                скидкой: {(this.item.price - ((this.item.price * +this.item.discount_percentage) / 100)).toFixed()}</Text>

              <TouchableOpacity activeOpacity={1} onPress={() => this.setValue('date_picker_start', true)}>
                <Input
                  editable={false}
                  ref={ref => this.discount_start_ref = ref}
                  placeholder="Начало скидки"
                  value={this.item.discount_start_on ? moment(this.item.discount_start_on).format('DD-MM-YYYY') : null}
                  leftIcon={{type: 'material-community', name: 'label-percent'}}
                />
              </TouchableOpacity>

              {this.date_picker_start && (
                <DateTimePicker
                  minimumDate={new Date()}
                  value={this.item.discount_start_on || new Date()}
                  mode={'date'}
                  display="default"
                  onChange={(e, v) => this.setDate('discount_start_on', v)}
                />
              )}

              <TouchableOpacity activeOpacity={1} onPress={() => this.setValue('date_picker_end', true)}>
                <Input
                  editable={false}
                  ref={ref => this.discount_end_ref = ref}
                  placeholder="Конец скидки"
                  value={this.item.discount_end_on ? moment(this.item.discount_end_on).format('DD-MM-YYYY') : null}
                  leftIcon={{type: 'material-community', name: 'label-percent'}}
                />
              </TouchableOpacity>

              {this.date_picker_end && (
                <DateTimePicker
                  minimumDate={this.item.discount_start_on || new Date()}
                  value={this.item.discount_end_on || this.item.discount_start_on || new Date()}
                  mode={'date'}
                  display="default"
                  onChange={(e, v) => this.setDate('discount_end_on', v)}
                />
              )}
            </>
          )}
        </Card>
        <Button
          title={'Сохранить'}
          loading={this.saving}
          disabled={!this.has_changes || this.saving || !this.item.is_valid_data}
          onPress={() => this.save()}
        />
      </View>
    );
  }
}

export default EditOfferScreen;
