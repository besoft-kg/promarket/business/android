import React from 'react';
import BaseScreen from './BaseScreen';
import {inject, observer} from 'mobx-react';
import {APP_PRIMARY_COLOR, requester} from '../utils/settings';
import {observable} from 'mobx';
import {ActivityIndicator, View} from 'react-native';
import BranchManagerScreen from './BranchManagerScreen';
import StoreManagerScreen from './StoreManagerScreen';

@inject('store') @observer
class HomeScreen extends BaseScreen {
  @observable loading = false;

  fetchItem = () => {
    if (this.loading) {
      return;
    }
    this.setValue('loading', true);
    requester.get('/to/home', {}, true).then((response) => {
      if (this.appStore.manager.is_branch) {
        this.offerStore.createOrUpdate(response.data);
      } else {
        this.offerStore.createOrUpdate(response.data.offers);
        this.branchStore.createOrUpdate(response.data.branches);
      }
    }).catch((e) => {
      console.log(e);
    }).finally(() => {
      this.setValue('loading', false);
    });
  };

  componentDidMount() {
    this.fetchItem();
  }

  render() {
    if (this.loading) {
      return (
        <View alignItems={'center'} flex={1} justifyContent={'center'}>
          <ActivityIndicator color={APP_PRIMARY_COLOR} size={34}/>
        </View>
      );
    }

    if (this.appStore.manager && this.appStore.manager.is_branch) {
      return <BranchManagerScreen />;
    }

    return <StoreManagerScreen />;
  }
}

export default HomeScreen;
