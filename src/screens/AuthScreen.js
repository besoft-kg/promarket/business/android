import React from "react";
import BaseScreen from "./BaseScreen";
import { Button, Card, Image, Input, Text } from "react-native-elements";
import { Alert, View } from "react-native";
import { APP_VERSION_CODE, requester } from "../utils/settings";
import { inject, observer } from "mobx-react";
import { action, observable } from "mobx";
import PhoneNumber from "../utils/PhoneNumber";

@inject("store") @observer
class AuthScreen extends BaseScreen {
  @observable status = "";
  @observable phone_number = "";
  @observable confirmation_code = "";
  @observable phone_number_e164 = "";

  @action sendConfirmationCode = async () => {
    if (!this.phone_number_e164 || this.appStore.authenticating) {
      return;
    }
    this.status = "code_is_sending";
    try {
      const response = await requester.post("/auth/phone", { phone_number: this.phone_number_e164.substr(1) });
      if (["code_is_sent", "code_was_sent"].includes(response.data.status)) {
        this.status = response.data.status;
        Alert.alert("", "Код подтверждения отправлен!");
      } else {
        this.status = "";
      }
      console.log(response);
    } catch (e) {
      this.status = "";
      Alert.alert("Ошибка!", e.response.data.message);
    }
  };

  @action setPhoneNumber = e => {
    this.phone_number = e;
    const phone = new PhoneNumber();
    phone.parseFromString(e);
    if (phone.isValid()) {
      this.phone_number_e164 = phone.getE164Format();
    } else {
      this.phone_number_e164 = "";
    }
  };

  @action checkConfirmationCode = async () => {
    if (this.confirmation_code.length !== 4 || this.appStore.authenticating) {
      return;
    }
    this.status = "code_is_checking";
    this.appStore.setValue('authenticating', true);
    try {
      const response = await requester.post("/auth/phone/check", {
        phone_number: this.phone_number_e164.substr(1),
        verification_code: this.confirmation_code,
        version_code: APP_VERSION_CODE,
      });
      this.appStore.signIn(response.data);
      Alert.alert('', `Добро пожаловать, ${response.data.user.full_name}`);
    } catch (e) {
      console.log(e);
      console.log(e.response);
      Alert.alert("Ошибка!", e.response.data.message);
      if (["code_is_incorrect", "code_is_invalid"].includes(e.response.data.status)) {
        this.status = e.response.data.status === "code_is_incorrect" ? "code_is_sent" : "";
        this.confirmation_code = "";
      } else {
        this.status = "code_is_sent";
      }
    } finally {
      this.appStore.setValue('authenticating', false);
    }
  };

  render() {
    return (
      <View flex={1} style={{ margin: 8 }} justifyContent={"flex-end"} alignItems={"center"}>
        <View flex={1} justifyContent={'center'} alignItems={'center'}>
          <Image style={{width: 100, height: 100}} resizeMode={'contain'} source={require('../assets/images/logo-274-304.png')}/>
          <Text style={{marginVertical: 8,textTransform: 'uppercase',color: '#ef7f1a',fontWeight: 'bold',fontSize: 40}}>
           Pro<Text style={{color: '#388e3c',fontWeight: 'bold'}}>Market</Text>
          </Text>
          <Text style={{textTransform: 'uppercase',color: '#388e3c',fontWeight: 'bold',fontSize: 25}}>Business</Text>
        </View>
        <Card containerStyle={{ width: "100%" }}>
          <Input
            disabled={["code_is_checking", "code_is_sending", "code_is_sent", "code_was_sent"].includes(this.status)}
            keyboardType={"phone-pad"}
            onChangeText={e => this.setPhoneNumber(e)}
            inputContainerStyle={{ width: "100%" }}
            placeholder={"Телефон номер"}
            leftIcon={{ type: "material-community", name: "phone" }}
          />
          {["code_is_sent", "code_was_sent", "code_is_checking"].includes(this.status) ? (
            <>
              <Input
                onChangeText={e => this.setValue("confirmation_code", e)}
                keyboardType={"numeric"}
                disabled={["code_is_checking"].includes(this.status)}
                inputContainerStyle={{ width: "100%" }}
                placeholder={"Код подтверждения"}
                leftIcon={{ type: "material-community", name: "lock" }}
              />
              <Button
                disabled={this.confirmation_code.length !== 4 || ["code_is_checking"].includes(this.status)}
                onPress={() => this.checkConfirmationCode()}
                title="Проверить код"
              />
            </>
          ) : (
            <Button
              loading={this.status === "code_is_sending"}
              disabled={!this.phone_number_e164 || this.status === "code_is_sending"}
              onPress={() => this.sendConfirmationCode()}
              title="Отправить код"
            />
          )}
        </Card>
      </View>
    );
  }
}

export default AuthScreen;
