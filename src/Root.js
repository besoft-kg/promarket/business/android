import 'react-native-gesture-handler';
import React from 'react';
import {
  SafeAreaView,
  StatusBar,
} from 'react-native';
import App from './App';
import RootStore from './stores/RootStore';
import {Provider} from 'mobx-react';
import {NavigationContainer} from '@react-navigation/native';
import {OverflowMenuProvider} from 'react-navigation-header-buttons';

const Root: () => React$Node = () => {
  return (
    <>
      <OverflowMenuProvider>
        <Provider store={RootStore}>
          <NavigationContainer>
            <StatusBar barStyle="dark-content"/>
            <SafeAreaView flex={1}>
              <App/>
            </SafeAreaView>
          </NavigationContainer>
        </Provider>
      </OverflowMenuProvider>
    </>
  );
};

export default Root;
