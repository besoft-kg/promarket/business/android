import React from 'react';
import BaseComponent from './components/BaseComponent';
import {ActivityIndicator, StatusBar, View} from 'react-native';
import {inject, observer} from 'mobx-react';
import {createStackNavigator} from '@react-navigation/stack';
import AuthScreen from './screens/AuthScreen';
import {APP_PRIMARY_COLOR} from './utils/settings';
import HomeStack from './stacks/HomeStack';
import moment from 'moment';
import 'moment/locale/ru';
import storage from './utils/storage';

moment.locale('ru');
const Stack = createStackNavigator();

@inject('store') @observer
class App extends BaseComponent {
  constructor(props) {
    super(props);

    Promise.all([
      storage.get('token', null),
      storage.get('store', null),
      storage.get('fcm_token', null),
      storage.get('categories', []),
      storage.get('branch', null),
      storage.get('manager', null),
      storage.get('user', null),
    ]).then(values => {
      this.store.setDataFromStorage(values);
      //this.checkLastVersion();
      this.appStore.checkAuth();
    });
  }

  render() {
    return (
      <>
        <StatusBar barStyle={this.appStore.authenticated ? 'light-content' : 'dark-content'} translucent={!this.appStore.authenticated} backgroundColor={this.appStore.authenticated ? APP_PRIMARY_COLOR : 'transparent'}/>

        {!this.appStore.app_is_ready ? (
          <View flex={1} alignItems={'center'} justifyContent={'center'}>
            <ActivityIndicator color={APP_PRIMARY_COLOR} size={'large'}/>
          </View>
        ) : (
          <Stack.Navigator
            mode={'modal'}
            screenOptions={{
              headerShown: false,
            }}
          >
            {/*<Stack.Screen*/}
            {/*  name={'ProductImageModal'}*/}
            {/*  component={ProductImageModal}*/}
            {/*/>*/}

            {this.appStore.authenticated ? (
              <>
                <Stack.Screen name={'HomeStack'} component={HomeStack}/>
              </>
            ) : (
              <Stack.Screen name={'AuthScreen'} component={AuthScreen}/>
            )}
          </Stack.Navigator>
        )}
      </>
    );
  }
}

export default App;
